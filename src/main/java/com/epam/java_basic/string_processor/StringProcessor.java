package com.epam.java_basic.string_processor;

import com.epam.java_basic.View;

/**
 * Useful methods for string processing
 */
public class StringProcessor {
    private View view;

    public StringProcessor() {
        this.view = new View();
    }

    public String findShortestLine(String[] lines) {
        String shortLine = null;
        for (int i = 0; i < lines.length; i++) {
            if (shortLine == null || lines[i].length() < shortLine.length()) {
                shortLine = lines[i];
            }
        }
        return shortLine;
    }

    public String findLongestLine(String[] lines) {
        String longLine = null;
        for (int i = 0; i < lines.length; i++) {

            if (longLine == null || lines[i].length() > longLine.length()) {
                longLine = lines[i];
            }
        }
        return longLine;
    }

    public String[] findLinesShorterThanAverageLength(String[] lines) {
        String[] result = new String[0];
        float average = getAverage(lines);
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].length() < average) {
                result = stringToArray(result, lines[i]);
            }
        }
        return result;
    }

    public String[] findLinesLongerThanAverageLength(String[] lines) {
        String[] result = new String[0];
        float average = getAverage(lines);
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].length() > average) {
                result = stringToArray(result, lines[i]);
            }

        }
        return result;
    }

    public float getAverage(String[] lines) {
        float average = 0;
        for (String item : lines) {
            average += (float) item.length();
        }
        average = average / lines.length;
        return average;
    }

    private String[] stringToArray(String[] line, String s) {   // на вход идёт массив и строка, которую мы добавляем к новому массиву
        String[] result = new String[line.length + 1];
        System.arraycopy(line, 0, result, 0, line.length);
        result[result.length - 1] = s;
        return result;
    }

    /**
     * Find word with minimum various characters. Return first word if there are a few of such words.
     *
     * @param words Input array of words
     * @return First word that consist of minimum amount of various characters
     */
    public String findWordWithMinimumVariousCharacters(String[] words) {
        String result;
        if (words.length > 0) {
            result = words[0];
            for (int i = 1; i < words.length; i++) {
                if (diffWords(words[i]) < diffWords(result))
                    result = words[i];
            }
        }else {
            return null;
        }
        return result;
    }

    public String findWordConsistingOfVariousCharacters(String[] words) {
        for (String s : words) {
            if (wordHasUncialOnlyCharacter(s)) {

                return s;
            }
        }
        return null;
    }

    /**
     * Find word containing only of digits. Return second word if there are a few of such words.
     *
     * @param words Input array of words
     * @return Second word that containing only of digits
     */
    public String findWordConsistingOfDigits(String[] words) {
        String result = null;
        int tempNumber = 0;
        for (int i = 0; i < words.length; i++) {
            if (parseStringToInteger(words[i]) != null) {
                result = words[i];
                tempNumber++;

            }
            if (tempNumber == 2)
                break;
        }
        return result;
    }

    private int diffWords(String st) {
        StringBuilder temp = new StringBuilder();
        String c; // текущий символ в строке
        for (int i = 0; i < st.length(); i++) {
            c = String.valueOf(st.charAt(i));
            if (temp.indexOf(c) == -1) //  если символ еще не встречался ранее
                temp.append(c); // добавляем в буфер
        }
        return temp.length();
    }

    private boolean wordHasUncialOnlyCharacter(String word) {
        StringBuilder temp = new StringBuilder();
        String c; // текущий символ в строке
        for (int i = 0; i < word.length(); i++) {
            c = String.valueOf(word.charAt(i));
            if (temp.indexOf(c) == -1) { //  если символ уникальный кладём символ в билдер, если встречается повторное значение, возвращаем false
                temp.append(c);
                continue;
            }
            return false;
        }
        return true;
    }

    private Long parseStringToInteger(String word) {
        try {
            return Long.valueOf(word);
        } catch (Exception e) {
            return null;
        }
    }

}

