package com.epam.java_basic;

import java.util.Arrays;

public class View {
    public static final String LINE = "line\n";
    public static final String WORD = "word\n";
    public static final String INPUT_MESSAGE = "Input ";
    public static final String SPECIFY_AMOUNT_OF = "\nSpecify amount of ";
    public static final String MESSAGE_ABOVE_ZERO = "Только числа больше нуля \n";
    public static final String MESSAGE_ONLY_INTEGER = "Целочисленные значения \n";
    public static final String MESSAGE_LENGTH = " length is ";
    public static final String MESSAGE_1 = "1) Shortest line is ";
    public static final String MESSAGE_2 = "2) Longest line is ";
    public static final String MESSAGE_3 = "3) Lines shorter than average length: \n";
    public static final String MESSAGE_4 = "\n4) Lines longer than average length: \n";
    public static final String MESSAGE_5 = "5) Word with minimum various characters: ";
    public static final String MESSAGE_6 = "6) Word contains only various characters: ";
    public static final String MESSAGE_7 = "7) Word contains only digits: ";


    public void printEmptyString() {
        System.out.println();
    }

    public void printMessage(String a) {
        System.out.print(a);
    }

    public void printInt(int a) {
        System.out.println(a);
    }

    public void printDataType(String a) {
        System.out.print(a);
    }

    public void printString(String a) {
        System.out.print(a);
    }

    public void printString(String[] a) {
        System.out.print(Arrays.toString(a));
    }


}
