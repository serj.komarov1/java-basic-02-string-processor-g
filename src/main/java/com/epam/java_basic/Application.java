package com.epam.java_basic;

import com.epam.java_basic.string_processor.StringProcessor;

import java.util.Scanner;

/**
 * Application's entry point, use it to demonstrate your code execution
 */
public class Application { //Комаров Сергей задание 2
    private View view;
    private StringProcessor stringProcessor;

    public Application() {
        this.view = new View();
        this.stringProcessor = new StringProcessor();
    }


    public static void main(String[] args) {
        Application application = new Application();
        application.start();
    }

    public void start() {
        view.printMessage(View.SPECIFY_AMOUNT_OF);
        view.printDataType(View.LINE);
        String[] line = fillArrayWithData(View.LINE);

        view.printMessage(View.MESSAGE_1);
        view.printMessage(stringProcessor.findShortestLine(line));
        view.printMessage(view.MESSAGE_LENGTH);
        view.printInt((stringProcessor.findShortestLine(line)).length());

        view.printMessage(view.MESSAGE_2);
        view.printMessage(stringProcessor.findLongestLine(line));
        view.printMessage(view.MESSAGE_LENGTH);
        view.printInt((stringProcessor.findLongestLine(line)).length());

        view.printMessage(view.MESSAGE_3);
        view.printString(stringProcessor.findLinesShorterThanAverageLength(line));
        view.printMessage(view.MESSAGE_4);

        view.printString(stringProcessor.findLinesLongerThanAverageLength(line));

        view.printMessage(View.SPECIFY_AMOUNT_OF);
        view.printDataType(View.WORD);
        String[] word = fillArrayWithData(View.WORD);

        view.printMessage(view.MESSAGE_5);
        view.printMessage(stringProcessor.findWordWithMinimumVariousCharacters(word));
        view.printEmptyString();

        view.printMessage(view.MESSAGE_6);
        view.printMessage(stringProcessor.findWordConsistingOfVariousCharacters(word));
        view.printEmptyString();

        view.printMessage(view.MESSAGE_7);
        view.printMessage(stringProcessor.findWordConsistingOfDigits(word));
    }


    public String[] fillArrayWithData(String a) {                            //метод для ввода строк или слов
        Scanner sc = new Scanner(System.in);
        Integer wordCount;
        do {
            wordCount = getCount(sc);
        } while (wordCount == null);

        String[] data = new String[wordCount];

        for (int i = 0; i < wordCount; i++) {
            view.printMessage(view.INPUT_MESSAGE);
            view.printMessage(a);
            data[i] = sc.nextLine();
        }
        return data;
    }

    private Integer getCount(Scanner sc) {                     //метод получения количества строк или слов с проверкой

        String number = sc.nextLine();
        try {
            Integer num = Integer.parseInt(number);
            if (num > 0) {
                return num;
            } else {
                view.printMessage(View.MESSAGE_ABOVE_ZERO);
                return null;
            }
        } catch (Exception ex) {
            view.printMessage(View.MESSAGE_ONLY_INTEGER);
            return null;
        }
    }
}